import argparse
import logging
from services.ExifService import ExifService

import logger

from app import app
from camera.CaptureParameters import CaptureParameters
from camera.controller import Controller

# do your main stuff
if __name__ == '__main__':
    controller = Controller()

    logging.basicConfig(filename='controller.log', level=logging.INFO,
                        format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

    help_text = '''Bmetry camera controller V1.0'''
    parser = argparse.ArgumentParser(description=help_text)

    parser._action_groups.pop()
    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')

    required.add_argument("-c", dest="capture_mode", required=True, help="choose 'single', 'sequence' (with argument -t -> time in seconds), 'interval' to capture a sequence with argument -t, 'upload' to upload remaining images on the SD card")

    optional.add_argument("-t", dest="capture_duration", type=int, required=False,
                          help="seconds to burst capture e.g. 30 for half a minute of capturing or break between images in interval mode", default=None)

    optional.add_argument("-s", dest="shutterspeed", required=False, help="capture shutterspeed: \t\t" + str(list(CaptureParameters.SHUTTERSPEED.keys())), default=None)
    optional.add_argument("-a", dest="aperture", required=False, help="capture aperture: \t\t" + str(list(CaptureParameters.APERTURE.keys())), default=None)
    optional.add_argument("-i", dest="iso", required=False, help="capture iso: \t\t" + str(list(CaptureParameters.ISO.keys())), default=None)
    optional.add_argument("-imageformat", dest='imageformat', required=False, help='Set the image format: \t\t' + str(list(CaptureParameters.IMAGEFORMAT.keys())), default=None)
    optional.add_argument("-focus", dest="focus", required=False, help=" 'area' for area focusing with parameter -farea or 'auto' for autofocus or None for no_focus", default=None)
    optional.add_argument("-focus-area", dest="focus_area", required=False, help="input a focusarea as x and y value between 0 and 1, e.g. '0.6,0.1'", default=None)
    optional.add_argument("-break-time", dest="break_time", required=False, help="seconds of break time between images in interval mode", default=None)
    optional.add_argument("-image-count", dest="image_count", required=False, help="number of images to capture in interval mode", default=None)

    # Read passed arguments from command line

    args = parser.parse_args()

    #logging.info help




    capture_params = CaptureParameters(None, None, None, None, None, None, None, None, None).complete_with_defaults(args.focus, args.focus_area, args.imageformat, args.capture_duration, args.image_count, args.break_time, args.shutterspeed, args.aperture, args.iso)




    logging.info('\n \n \t  Bmetry camera controller V1.0    \n     ----------------------------------------')
    if (args.capture_mode == "single"):
        logging.info('   Single image capture')
        logging.info('   Shutterspeed: \t \t \t' + str(list(CaptureParameters.SHUTTERSPEED.keys())[list(CaptureParameters.SHUTTERSPEED.values()).index(capture_params.shutterspeed)]) + ' seconds')
        logging.info('   Aperture: \t \t \t \t' + str(list(CaptureParameters.APERTURE.keys())[list(CaptureParameters.APERTURE.values()).index(capture_params.aperture)]) + ' f')
        logging.info('   Iso: \t \t \t \t' + str(list(CaptureParameters.ISO.keys())[list(CaptureParameters.ISO.values()).index(capture_params.iso)]) + ' ')
        logging.info('   Focus: \t \t \t \t' + str('Autofocus:' + ' ' + capture_params.focus_mode))
        logging.info('   Imageformat: \t \t \t' + str(list(CaptureParameters.IMAGEFORMAT.keys())[list(CaptureParameters.IMAGEFORMAT.values()).index(capture_params.image_format)]))
        logging.info('\n \n')


    elif (args.capture_mode == "sequence"):
        logging.info('   Sequence image capture')
        logging.info('   Capture duration: \t \t \t' + str(args.capture_duration) + ' seconds')
        logging.info('   Shutterspeed: \t \t \t' + str(list(CaptureParameters.SHUTTERSPEED.keys())[list(CaptureParameters.SHUTTERSPEED.values()).index(capture_params.shutterspeed)]) + ' seconds')
        logging.info('   Aperture: \t \t \t \t' + str(list(CaptureParameters.APERTURE.keys())[list(CaptureParameters.APERTURE.values()).index(capture_params.aperture)]) + ' f')
        logging.info('   Iso: \t \t \t \t' + str(list(CaptureParameters.ISO.keys())[list(CaptureParameters.ISO.values()).index(capture_params.iso)]) + ' ')
        logging.info('   Focus: \t \t \t \t' + str('Autofocus:' + ' ' + capture_params.focus_mode))
        logging.info('   Imageformat: \t \t \t' + str(list(CaptureParameters.IMAGEFORMAT.keys())[list(CaptureParameters.IMAGEFORMAT.values()).index(capture_params.image_format)]))
        logging.info('\n \n')


    elif (args.capture_mode == "interval"):
        logging.info('   Interval image capture')
        logging.info('   Capture duration: \t \t \t' + str(args.break_time * args.image_count) + ' seconds')
        logging.info('   Break between images: \t \t \t' + str(args.break_time) + ' seconds')
        logging.info('   Image counter: \t \t \t ' + str(args.image_count) + ' images')
        logging.info('   Shutterspeed: \t \t \t' + str(list(CaptureParameters.SHUTTERSPEED.keys())[list(CaptureParameters.SHUTTERSPEED.values()).index(capture_params.shutterspeed)]) + ' seconds')
        logging.info('   Aperture: \t \t \t \t' + str(list(CaptureParameters.APERTURE.keys())[list(CaptureParameters.APERTURE.values()).index(capture_params.aperture)]) + ' f')
        logging.info('   Iso: \t \t \t \t' + str(list(CaptureParameters.ISO.keys())[list(CaptureParameters.ISO.values()).index(capture_params.iso)]) + ' ')
        logging.info('   Focus: \t \t \t \t' + str('Autofocus:' + ' ' + capture_params.focus_mode))
        logging.info('   Imageformat: \t \t \t' + str(list(CaptureParameters.IMAGEFORMAT.keys())[list(CaptureParameters.IMAGEFORMAT.values()).index(capture_params.image_format)]))
        logging.info('\n \n')

    elif (args.capture_mode == "upload"):
        logging.info('   Upload remaining images')
        logging.info('\n \n')

    camera = controller.getCamera()

    if args.capture_mode == 'upload':

        controller.download_camera_files_and_upload_to_ftp(camera, ExifService())
    else:
        #controller.camera_capture(args.capture_mode, args.focus, args.focus_area, args.imageformat, args.capture_duration, args.image_count, args.break_time, args.shutterspeed, args.aperture, args.iso)
        controller.camera_capture(args.capture_mode, capture_params, camera)

    controller.closeCamera(camera)

