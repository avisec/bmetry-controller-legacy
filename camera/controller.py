import argparse
import datetime
import logging
import logger
import os
import ftplib as ftp
from pathlib import Path
import configparser
import gphoto2 as gp
import time
import io
import exif
import subprocess
from exif import Image

from camera.CaptureParameters import CaptureParameters
from camera.PseudoFile import PseudoFile


# check plattform for GPIO operations
from services import ExifService


def is_raspberrypi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower(): return True
    except Exception:
        pass
    return False


if is_raspberrypi():
    import RPi.GPIO as GPIO
else:
    import os as GPIO


class Controller():

    def __init__(self):
        pass

    config_parser = configparser.RawConfigParser()
    config_parser.read('./application.properties')
    camera_power_pin = config_parser.get('bmetry', 'camera_power_bcm_pin')

    PHOTO_DIR = os.path.expanduser('./bmetry_images/')
    if (camera_power_pin != "" or camera_power_pin != None):
        GPIO_CAMERA_POWER = int(camera_power_pin)
    else:
        GPIO_CAMERA_POWER = 5


    # camera must be connected already
    def set_camera_widget(self, camera: gp.Camera, widget_name: str, widget_value):
        # get configuration tree
        config = gp.check_result(gp.gp_camera_get_config(camera))

        # set shutterspeed
        # find the shutterspeed config item
        widget = gp.check_result(
            gp.gp_widget_get_child_by_name(config, widget_name))
        # set value
        value = gp.check_result(gp.gp_widget_get_choice(widget, widget_value))
        gp.check_result(gp.gp_widget_set_value(widget, value))
        # set config
        gp.check_result(gp.gp_camera_set_config(camera, config))

    def get_camera_widget(self, camera: gp.Camera, widget_name: str):
        # get configuration tree
        config = gp.check_result(gp.gp_camera_get_config(camera))

        # find the widget config item
        widget = gp.check_result(
            gp.gp_widget_get_child_by_name(config, widget_name))
        # set value
        value = gp.check_result(
            gp.gp_widget_get_child_by_name(config, widget_name))
        return value


    """
    Must be set before initiating the camera
    """
    def set_datetime(self):
        bashCommand = "gphoto2 --set-config syncdatetime=1"
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()


    def get_target_dir(self, timestamp):
        return os.path.join(self.PHOTO_DIR, timestamp.strftime('%Y/'))

    def list_computer_files(self):
        result = []
        for root, dirs, files in os.walk(os.path.expanduser(self.PHOTO_DIR)):
            for name in files:
                if '.thumbs' in dirs:
                    dirs.remove('.thumbs')
                if name in ('.directory',):
                    continue
                ext = os.path.splitext(name)[1].lower()
                if ext in ('.db',):
                    continue
                result.append(os.path.join(root, name))
        return result

    def list_camera_files(self, camera, path='/'):
        result = []
        # get files
        gp_list = gp.check_result(
            gp.gp_camera_folder_list_files(camera, path))
        for name, value in gp_list:
            result.append(os.path.join(path, name))
        # read folders
        folders = []
        gp_list = gp.check_result(
            gp.gp_camera_folder_list_folders(camera, path))
        for name, value in gp_list:
            folders.append(name)
        # recurse over subfolders
        for name in folders:
            result.extend(self.list_camera_files(camera, os.path.join(path, name)))
        return result

    def get_camera_file_info(self, camera, path):
        folder, name = os.path.split(path)
        return gp.check_result(
            gp.gp_camera_file_get_info(camera, folder, name))

    def get_file_exif(self, camera, path):
        pf = PseudoFile(camera, path)
        return exif.process_file(pf)

    def download_camera_files_and_delete(self, camera: gp.Camera):
        callback_obj = gp.check_result(gp.use_python_logging())
        computer_files = self.list_computer_files()
        logging.info('Getting list of files from camera...')
        camera_files = self.list_camera_files(camera)
        if not camera_files:
            logging.info('No files found')
            return 1
        logging.info('Copying files...')
        for path in camera_files:
            info = self.get_camera_file_info(camera, path)
            timestamp = datetime.datetime.fromtimestamp(info.file.mtime)
            folder, name = os.path.split(path)
            dest_dir = self.get_target_dir(timestamp)
            dest = os.path.join(dest_dir, ((datetime.datetime.utcnow().strftime('%Y%m%dT%H%M%S.%f')[:-2]) + '.jpg'))
            if dest in computer_files:
                continue
            logging.info('%s -> %s' % (path, dest_dir))
            if not os.path.isdir(dest_dir):
                os.makedirs(dest_dir)
            camera_file = gp.check_result(gp.gp_camera_file_get(
                camera, folder, name, gp.GP_FILE_TYPE_NORMAL))
            gp.check_result(gp.gp_file_save(camera_file, dest))

            # delete file on camera
            gp.check_result(gp.gp_camera_file_delete(camera, folder, name))

        return dest_dir

    def upload_binary_to_ftp(self, camera_file, camera_file_name):
        try:
            # parse credentials
            ftp_config = configparser.RawConfigParser()
            ftp_config.read('./application.properties')
            session = ftp.FTP_TLS(ftp_config.get('FTP Credentials', 'server'),
                                  ftp_config.get('FTP Credentials', 'username'),
                                  ftp_config.get('FTP Credentials', 'password'))
            session.prot_p()

            logging.info('Uploading ' + camera_file_name)
            session.storbinary('STOR ' + camera_file_name, camera_file)  # send the file
            session.quit()
        except Exception:
            logging.info("Exception occurred during FTP upload:{}".format(Exception))
            return 1

        return 0

    def download_camera_files_and_upload_to_ftp(self, camera: gp.Camera, exifService: ExifService):
        controller_config = configparser.RawConfigParser()
        controller_config.read('./application.properties')
        capture_skip_factor=int(controller_config.get('bmetry','capture_skip_factor'))

        callback_obj = gp.check_result(gp.use_python_logging())
        computer_files = self.list_computer_files()
        logging.info('Getting list of files from camera...')
        camera_files = self.list_camera_files(camera)
        if not camera_files:
            logging.info('No files found')
            return 1
        logging.info('Copying files...')
        uploadable = True
        uploaded_file_counter = 0
        for num, path in enumerate(camera_files):
            #Do not skip the first image
            if num != 0:
                if (capture_skip_factor != 0):
                    if num % capture_skip_factor == 0:
                        uploadable = False
                    else: uploadable = True
                else: uploadable = True

            folder, name = os.path.split(path)
            file_info = self.get_camera_file_info(camera, path)
            if uploadable:

                # exif = get_file_exif(camera, path)
                # exif_date_time = exif['EXIF DateTimeOriginal'].values.decode('ascii')
                # exif_sub_sec_time = exif['EXIF SubSecTime'].values.decode('ascii')
                # date_time_str = str(exif_date_time + '.' + exif_sub_sec_time)
                # logging.info(date_time_str)
                # date_time_obj = datetime.datetime.strptime(date_time_str, '%Y:%m:%d %H:%M:%S.%f')

                # using Axis timestamp format
                # timestamp_name = 'bmetry' + date_time_obj.strftime('%y-%m-%d_%H-%M-%S-%f')[:-4] + '_00001' + '.jpg'
                # logging.info(timestamp_name)

                data = bytearray(file_info.file.size)
                view = memoryview(data)
                chunk_size = 1000 * 1024
                offset = 0
                while offset < len(data):
                    bytes_read = gp.check_result(gp.gp_camera_file_read(
                        camera, folder, name, gp.GP_FILE_TYPE_NORMAL,
                        offset, view[offset:offset + chunk_size]))
                    offset += bytes_read

                #Modify exif data
                try:
                    data = exifService.addCraneTag(data)
                except Exception as e:
                    logging.error(f"Failed to add crane tag: {e}")

                data_stream = io.BytesIO(data)



                ret = self.upload_binary_to_ftp(data_stream, name)
                if ret == 0:
                    # delete file on camera
                    uploaded_file_counter += 1
                    logging.info('Deleting %s on camera' % name)
                    gp.check_result(gp.gp_camera_file_delete(camera, folder, name))
                else:
                    ##try again
                    logging.info("Failed to upload, trying once more")
                    ret = self.upload_binary_to_ftp(data_stream, name)
                    if ret == 0:
                        # delete file on camera
                        uploaded_file_counter += 1
                        logging.info('Deleting %s on camera' % name)
                        gp.check_result(gp.gp_camera_file_delete(camera, folder, name))
                    else:
                        ##try again and again
                        logging.info("Failed to upload, trying another once more after waiting a bit")
                        time.sleep(10)
                        ret = self.upload_binary_to_ftp(data_stream, name)
                        if ret == 0:
                            # delete file on camera
                            uploaded_file_counter += 1
                            logging.info('Deleting %s on camera' % name)
                            gp.check_result(gp.gp_camera_file_delete(camera, folder, name))
                        else:
                            logging.info('Aborting after 3 trials, file remaining on camera')
                            break
            else:
                # delete file on camera
                logging.info('Deleting %s on camera due to capture_skip_factor = %s' % (name,capture_skip_factor))
                gp.check_result(gp.gp_camera_file_delete(camera, folder, name))
        # return number of images
        return uploaded_file_counter

    def camera_capture(self, capture_mode, focus_mode, focus_area, image_format, capture_duration, image_count,
                       break_time, shutterspeed, aperture, iso, exifService: ExifService):

        capture_params = CaptureParameters().complete_with_defaults(focus_mode, focus_area, image_format, capture_duration,
                                                             image_count,
                                                             break_time, shutterspeed, aperture, iso)

        camera = self.prepare_capture(capture_params)

        self.camera_capture(capture_mode, capture_params, camera, exifService)

    def camera_capture(self, capture_mode, capture_params: CaptureParameters, camera, exifService: ExifService):

        # single mode
        if capture_mode == "single":
            self.capture_single(camera)
            logging.info("Wait for camera to finish...")
            time.sleep(2)
        elif capture_mode == "sequence":
            self.capture_sequence(camera, capture_params)
            logging.info("Wait for camera to finish...")
            time.sleep(15)
        elif capture_mode == "interval":
            self.capture_interval(camera, capture_params, exifService)
            logging.info("Wait for camera to finish...")
            time.sleep(15)



        # reset drivemode to single image
        self.set_camera_widget(camera, 'drivemode', 0)

        # download files from camera and delete
        # image_dest_dir = download_camera_files_and_delete(camera)

        # download files from camera and upload
        self.closeCamera(camera)
        time.sleep(2)
        new_camera = self.getCamera()
        nbr_of_files = self.download_camera_files_and_upload_to_ftp(new_camera, exifService)

        self.closeCamera(new_camera)

        return nbr_of_files

        # return image_dest_dir

    def prepare_capture(self, focus_mode, focus_area, image_format, capture_duration, image_count,
                        break_time, shutterspeed, aperture, iso):
        # defaults
        capture_params = CaptureParameters().complete_with_defaults(focus_mode, focus_area, image_format, capture_duration,
                                                             image_count,
                                                             break_time, shutterspeed, aperture, iso)

        self.prepare_capture(capture_params)

    def prepare_capture(self, capture_params: CaptureParameters):

        callback_obj = gp.check_result(gp.use_python_logging())
        # open camera connection
        camera = self.getCamera()

        # set shutterspeed
        self.set_camera_widget(camera, 'shutterspeed', capture_params.shutterspeed)
        # set aperture
        self.set_camera_widget(camera, 'aperture', capture_params.aperture)
        # set iso
        self.set_camera_widget(camera, 'iso', capture_params.iso)

        # set imageformat
        self.set_camera_widget(camera, 'imageformat', capture_params.image_format)

        # Do an auto focus if requested
        if capture_params.focus_mode == 'auto':
            self.set_camera_widget(camera, 'focusmode', 0)
            self.set_camera_widget(camera, 'eosremoterelease', 6)
            time.sleep(1)
            self.set_camera_widget(camera, 'eosremoterelease', 9)

        if capture_params.focus_mode == 'area':
            # set to single auto focus
            self.set_camera_widget(camera, 'focusmode', 0)

            # set zoom coordinates
            x = int(round(float(capture_params.focus_area.split(',')[0]) * 5250))
            y = int(round(float(capture_params.focus_area.split(',')[1]) * 3500))

            logging.info("Area focusing on image coordinates {},{}".format(x,y))

            config = gp.check_result(gp.gp_camera_get_config(camera))
            widget = gp.check_result(gp.gp_widget_get_child_by_name(config, 'eoszoomposition'))
            gp.check_result(gp.gp_widget_set_value(widget, str(str(x) + ',' + str(y))))
            gp.check_result(gp.gp_camera_set_config(camera, config))

            # set_camera_widget(camera, 'eoszoomposition', str(str(x) + ',' + str(y)))  # 'Memory Card')  # or 1
            self.set_camera_widget(camera, 'eosremoterelease', 6)  # 'Memory Card')  # or 1
            self.set_camera_widget(camera, 'eosremoterelease', 4)  # 'Memory Card')  # or 1

        # needed to set foucs mode Manual, Manual is not officially a supported focus mode
        # get configuration tree
        config = gp.check_result(gp.gp_camera_get_config(camera))
        widget = gp.check_result(gp.gp_widget_get_child_by_name(config, 'focusmode'))
        gp.check_result(gp.gp_widget_set_value(widget, 'Manual'))
        gp.check_result(gp.gp_camera_set_config(camera, config))

        # set capture target to Card
        self.set_camera_widget(camera, 'capturetarget', 1)  # 'Memory Card')  # or 1
        return camera

    # set settings, capture mode, capture, download image, clean up camera
    def capture_single(self, camera):

        # single mode
        # set capture mode to single image mode
        self.set_camera_widget(camera, 'drivemode', 0)
        gp.check_result(gp.gp_camera_capture(camera, gp.GP_CAPTURE_IMAGE))

        logging.info("Wait for camera to finish...")
        time.sleep(2)

    # set settings, capture mode, capture, download image, clean up camera
    def capture_sequence(self, camera, capture_params):

        # set capture mode to low speed burst
        self.set_camera_widget(camera, 'drivemode', 3)  # 'Continuous low speed')  # or 3

        # start capturing
        logging.info('Starting capture sequence \n')
        stop_capture_time = capture_params.capture_duration + time.time()
        self.set_camera_widget(camera, 'eosremoterelease', 5)  # Immediate
        while True:
            remaining = round(stop_capture_time - time.time())
            logging.info('Capturing for ' + str(remaining))
            time.sleep(1)
            if time.time() >= stop_capture_time:
                self.set_camera_widget(camera, 'eosremoterelease', 4)  # Release Full
                break

    # set settings, capture mode, capture, download image, clean up camera
    def capture_interval(self, camera, capture_params, exifService):
        # set capture mode to single image mode
        self.set_camera_widget(camera, 'drivemode', 0)

        # upload after all images if less than 12 seconds break
        if capture_params.break_time < 12:
            for count in range(1, capture_params.image_count + 1):
                start_time = time.time()
                logging.info('\nCapturing %d of %d' % (count, capture_params.image_count))
                gp.check_result(gp.gp_camera_capture(camera, gp.GP_CAPTURE_IMAGE))
                time_to_sleep = capture_params.break_time - (time.time() - start_time)
                if time_to_sleep <= 0:
                    continue
                else:
                    logging.info("Waiting for " + str(int(time_to_sleep)) + ' seconds')
                    time.sleep(time_to_sleep)

        # upload after each image if more than 12 seconds break
        else:
            for count in range(1, capture_params.image_count + 1):
                start_time = time.time()
                logging.info('\nCapturing %d of %d' % (count, capture_params.image_count))
                gp.check_result(gp.gp_camera_capture(camera, gp.GP_CAPTURE_IMAGE))
                time.sleep(1)
                logging.info("Wait for camera to finish...")
                # download files from camera and upload
                nbr_of_files = self.download_camera_files_and_upload_to_ftp(camera, exifService)
                time_to_sleep = capture_params.break_time - (time.time() - start_time)
                if time_to_sleep <= 0:
                    continue
                else:
                    logging.info("Waiting for " + str(int(time_to_sleep)) + ' seconds')
                    time.sleep(time_to_sleep)

            logging.info('Finished interval capture')
            return nbr_of_files

    # upload image files
    def upload_files_in_dir(self, image_dir):
        # file_list = os.listdir(image_dir)
        file_list = sorted(Path(image_dir).iterdir())

        # parse credentials
        ftp_config = configparser.RawConfigParser()
        ftp_config.read('./application.properties')
        for item in file_list:
            # needed to open a new session for each file to stay in the limitation of 1 connection per user
            session = ftp.FTP_TLS(ftp_config.get('FTP Credentials', 'server'),
                                  ftp_config.get('FTP Credentials', 'username'),
                                  ftp_config.get('FTP Credentials', 'password'))
            session.prot_p()
            file = open(str(item), 'rb')
            logging.info('Uploading ' + str(item.name))
            session.storbinary('STOR ' + item.name, file)  # send the file
            session.quit()
            file.close()
            logging.info('Deleting ' + str(item.name))
            os.remove(str(item))

    def print_dict(self, dict):
        for key in dict.keys():
            logging.info(key, end=', ')

    def getCamera(self):
        if (is_raspberrypi()):
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.GPIO_CAMERA_POWER, GPIO.OUT)
            GPIO.output(self.GPIO_CAMERA_POWER, GPIO.HIGH)
            time.sleep(3)

        self.set_datetime()
        camera = gp.check_result(gp.gp_camera_new())
        gp.check_result(gp.gp_camera_init(camera))
        return camera

    def closeCamera(self, camera):
        gp.gp_camera_exit(camera)
        if (is_raspberrypi()):
            GPIO.output(self.GPIO_CAMERA_POWER, GPIO.LOW)

    def upload_remaining_images(self, exifService):
        self.download_camera_files_and_upload_to_ftp(self.getCamera(), exifService)
