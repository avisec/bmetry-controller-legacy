import configparser

import logger
from pathlib import Path
import logging

class CaptureParameters:


    config_parser = configparser.RawConfigParser()
    #config_parser.read(str(Path.home()) + '/application.properties')
    config_parser.read('./application.properties')
    default_shutterspeed = config_parser.get('Exposure Parameters', 'shutterspeed')
    default_aperture= config_parser.get('Exposure Parameters', 'aperture')
    default_iso = config_parser.get('Exposure Parameters', 'iso')
    default_focus_area = config_parser.get('bmetry', 'focus_area')



    SHUTTERSPEED = {
        # shutterspeeds should be below 0.3 seconds to capture at 2.9fps
        # "bulb": 0,
        # "30": 1,
        # "25": 2,
        # "20": 3,
        # "15": 4,
        # "13": 5,
        # "10.3": 6,
        # "8": 7,
        # "6.3": 8,
        # "5": 9,
        # "4": 10,
        # "3.2": 11,
        # "2.5": 12,
        # "2": 13,
        # "1.6": 14,
        # "1.3": 15,
        # "1": 16,
        # "0.8": 17,
        # "0.6": 18,
        # "0.5": 19,
        # "0.4": 20,
        "0.3": 21,
        "1/4": 22,
        "1/5": 23,
        "1/6": 24,
        "1/8": 25,
        "1/10": 26,
        "1/13": 27,
        "1/15": 28,
        "1/20": 29,
        "1/25": 30,
        "1/30": 31,
        "1/40": 32,
        "1/50": 33,
        "1/60": 34,
        "1/80": 35,
        "1/100": 36,
        "1/125": 37,
        "1/160": 38,
        "1/200": 39,
        "1/250": 40,
        "1/320": 41,
        "1/400": 42,
        "1/500": 43,
        "1/640": 44,
        "1/800": 45,
        "1/1000": 46,
        "1/1250": 47,
        "1/1600": 48,
        "1/2000": 49,
        "1/2500": 50,
        "1/3200": 51,
        "1/4000": 52

    }

    APERTURE = {
        "1.4": 1,
        "1.6": 2,
        "1.8": 3,
        "2": 4,
        "2.2": 5,
        "2.5": 6,
        "2.8": 7,
        "3.2": 8,
        "3.5": 9,
        "4": 10,
        "4.5": 11,
        "5": 12,
        "5.6": 13,
        "6.3": 14,
        "7.1": 15,
        "8": 16,
        "9": 17,
        "10": 18,
        "11": 19,
        "13": 20,
        "14": 21,
        "16": 22,
    }

    ISO = {
        "Auto": 0,
        "100": 1,
        "125": 2,
        "160": 3,
        "200": 4,
        "250": 5,
        "320": 6,
        "400": 7,
        "500": 8,
        "640": 9,
        "800": 10,
        "1000": 11,
        "1250": 12,
        "1600": 13,
        "2000": 14,
        "2500": 15,
        "3200": 16,
        "4000": 17,
        "5000": 18,
        "6400": 19,
        "8000": 20,
        "10000": 21,
        "12800": 22,
        "16000": 23,
        "20000": 24,
        "25600": 25,
        # "Unknown value 0001": 26
    }

    # resolution
    IMAGEFORMAT = {
        "Large Fine JPEG": 0,
        "Large Normal JPEG": 1,
        "Medium Fine JPEG": 2,
        "Medium Normal JPEG": 3,
        "Small Fine JPEG": 4,
        "Small Normal JPEG": 5,
        "Smaller JPEG": 6,
    }

    focus_mode: str
    focus_area: str
    image_format: int
    capture_duration: int
    image_count: int
    break_time: int
    shutterspeed: int
    aperture: int
    iso: int




    def __init__(self, focus_mode, focus_area, image_format, capture_duration, image_count,
                 break_time, shutterspeed, aperture, iso):

            self.set_defaults()
            self.focus_mode = focus_mode
            self.focus_area = focus_area
            self.image_format = image_format
            self.capture_duration = capture_duration
            self.image_count = image_count
            self.break_time = break_time
            self.shutterspeed = shutterspeed
            self.aperture = aperture
            self.iso = iso


    def complete_with_defaults(self, focus_mode, focus_area, image_format, capture_duration, image_count,
                 break_time, shutterspeed, aperture, iso):
        if focus_mode is None:
            self.focus_mode = 'no_focus'
        else:
            self.focus_mode = focus_mode

        if focus_area is None:
            self.focus_area = self.default_focus_area
        else:
            self.focus_area = focus_area

        if image_format is None:
            self.image_format = self.IMAGEFORMAT['Large Fine JPEG']
        else:
            self.image_format = self.IMAGEFORMAT[image_format]

        if capture_duration is None:
            self.capture_duration = 5
        else:
            self.capture_duration = int(capture_duration)

        if image_count is None:
            self.image_count = 10
        else:
            self.image_count = int(image_count)

        if break_time is None:
            self.break_time = 15
        else:
            self.break_time = int(break_time)

        if shutterspeed is None:
            self.shutterspeed = self.SHUTTERSPEED[self.default_shutterspeed]
        else:
            self.shutterspeed = self.SHUTTERSPEED[shutterspeed]

        if aperture is None:
            self.aperture = self.APERTURE[self.default_aperture]
        else:
            self.aperture = self.APERTURE[aperture]

        if iso is None:
            self.iso = self.ISO[self.default_iso]
        else:
            self.iso = self.ISO[iso]

        return self



    def set_defaults(self):
        self.focus_mode = 'no_focus'
        self.focus_area = self.default_focus_area
        self.image_format = self.IMAGEFORMAT['Large Fine JPEG']
        self.capture_duration = 5
        self.image_count = 10
        self.break_time = 15
        self.shutterspeed = self.SHUTTERSPEED[self.default_shutterspeed]
        self.aperture = self.APERTURE[self.default_aperture]
        self.iso = self.ISO[self.default_iso]
        return self






