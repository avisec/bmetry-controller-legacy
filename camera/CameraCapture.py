#from __future__ import logging.info_function

import logging
import logger
import os
import subprocess
import sys

import gphoto2 as gp


class Camera:
    def __init__(self):
        self.camera = gp.gp_camera_new()

    # connect to a camera device and return the camera instance
    def connect_camera(self):
        logging.basicConfig(
            format='%(levelname)s: %(name)s: %(message)s', level=logging.WARNING)
        callback_obj = gp.check_result(gp.use_python_logging())
        camera = gp.Camera()
        gp.check_result(gp.gp_camera_init(camera))
        return camera

    # close the connection to a camera
    def disconnect_camera(self):
        gp.gp_camera_exit(self.camera)
        return 0

    # set a config on a given camera instance and setting field
    def set_camera_config_value(self, widget_name: str, widget_value):
        # use Python logging
        logging.basicConfig(
            format='%(levelname)s: %(name)s: %(message)s', level=logging.WARNING)
        callback_obj = gp.check_result(gp.use_python_logging())
        # open camera connection
        camera = self.connect_camera()
        # get configuration tree
        config = gp.check_result(gp.gp_camera_get_config(camera))
        # find the capture target config item
        gphoto_widget = gp.check_result(
            gp.gp_widget_get_child_by_name(config, widget_name))
        if type(widget_value) is int:
            # check value in range
            count = gp.check_result(gp.gp_widget_count_choices(gphoto_widget))
            if widget_value < 0 or widget_value >= count:
                logging.info('Parameter out of range')
                return 1
            # set value
            value = gp.check_result(gp.gp_widget_get_choice(gphoto_widget, widget_value))
            gp.check_result(gp.gp_widget_set_value(gphoto_widget, value))
        else:
            # set value
            gp.check_result(gp.gp_widget_set_value(gphoto_widget, widget_value))
        # set config
        gp.check_result(gp.gp_camera_set_config(camera, config))
        # clean up
        gp.check_result(self.disconnect_camera(camera))
        return 0

    # read a config from a give camera instance an setting field
    def read_camera_config_value(self, widget_name: str):
        # use Python logging
        logging.basicConfig(
            format='%(levelname)s: %(name)s: %(message)s', level=logging.WARNING)
        callback_obj = gp.check_result(gp.use_python_logging())
        # open camera connection
        camera = self.connect_camera()
        # get configuration tree
        config = gp.check_result(gp.gp_camera_get_config(camera))
        # find the capture target config item
        gphoto_widget = gp.check_result(
            gp.gp_widget_get_child_by_name(config, widget_name))
        # get config
        gp.check_result(gp.gp_camera_get_config(camera, config))
        # clean up
        return gp.gp_widget_get_value(gphoto_widget)

    # capture an image on a camera instance and download it to the host machine
    def capture(self):
        logging.basicConfig(
            format='%(levelname)s: %(name)s: %(message)s', level=logging.WARNING)
        camera = self.connect_camera()
        logging.info('Capturing image')
        file_path = camera.capture(gp.GP_CAPTURE_IMAGE)
        logging.info('Camera file path: {0}/{1}'.format(file_path.folder, file_path.name))
        target = os.path.join('/tmp', file_path.name)
        logging.info('Copying image to', target)
        camera_file = camera.file_get(
            file_path.folder, file_path.name, gp.GP_FILE_TYPE_NORMAL)
        camera_file.save(target)
        subprocess.call(['xdg-open', target])
        camera.exit()
        return 0





#camera = Camera()
#camera.set_camera_config_value("iso", 5)
#camera.disconnect_camera()

