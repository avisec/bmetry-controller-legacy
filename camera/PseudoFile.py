import os
import gphoto2 as gp


class PseudoFile(object):
    def __init__(self, camera, path):
        self._camera = camera
        self._folder, self._file_name = os.path.split(path)
        info = self._camera.file_get_info(
            self._folder, self._file_name)
        self._size = info.file.size
        self._ptr = 0
        self._buf = bytearray(16 * 1024)
        self._buf_ptr = 0
        self._buf_len = 0

    def read(self, size=None):
        if size is None or size < 0:
            size = self._size - self._ptr
        if (self._ptr < self._buf_ptr or
                self._ptr >= self._buf_ptr + self._buf_len):
            self._buf_ptr = self._ptr - (self._ptr % len(self._buf))
            self._buf_len = self._camera.file_read(
                self._folder, self._file_name, gp.GP_FILE_TYPE_NORMAL,
                self._buf_ptr, self._buf)
        offset = self._ptr - self._buf_ptr
        size = min(size, self._buf_len - offset)
        self._ptr += size
        return self._buf[offset:offset + size]

    def seek(self, offset, whence=0):
        if whence == 0:
            self._ptr = offset
        elif whence == 1:
            self._ptr += offset
        else:
            self._ptr = self._size - self.ptr

    def tell(self):
        return self._ptr