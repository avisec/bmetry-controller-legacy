from __future__ import print_function

import logging
import os
# from dataclasses import dataclass

import qwiic_icm20948 as Imu
import time
import sys
import math
import csv
import datetime



"""
@dataclass
class ImuData:
    timestamp: datetime.datetime
    # [G -> acceleration on earth]
    axRaw: int
    ayRaw: int
    azRaw: int
    # [ dps -> degrees per second]
    gxRaw: int
    gyRaw: int
    gzRaw: int
    #  [uT -> micro Tesla]
    mxRaw: int
    myRaw: int
    mzRaw: int
"""

class ImuLogger():

    def __init__(self, log_file_path: str):
        self.imu = Imu.QwiicIcm20948()
        self.init_imu(self.imu)
        self.file_path = log_file_path


    def init_imu(self, imu: Imu.QwiicIcm20948):
        if not imu.connected:
            logging.error("The Qwiic ICM20948 device isn't connected to the system. Please check your connection", file=sys.stderr)
            return

        imu.begin()
        # detect +- 8G with a resolution of 4096 LSB/g
        # imu.setFullScaleRangeAccel(0x02)
        # detect +- 1000 dps (degree per second) with resolution of 32.8 LSB/dps
        # imu.setFullScaleRangeGyro(0x02)


    def get_imu_data(self) -> dict:
        sampling_rate = 1
        avgAx = 0
        avgAy = 0
        avgAz = 0
        avgGx = 0
        avgGy = 0
        avgGz = 0
        avgMx = 0
        avgMy = 0
        avgMz = 0

        #for number in range(1, sampling_rate):
        if self.imu.dataReady():
            self.imu.getAgmt()
            avgAx += self.imu.axRaw
            avgAy += self.imu.ayRaw
            avgAz += self.imu.azRaw
            avgGx += self.imu.gxRaw
            avgGy += self.imu.gyRaw
            avgGz += self.imu.gzRaw
            avgMx += self.imu.mxRaw
            avgMy += self.imu.myRaw
            avgMz += self.imu.mzRaw
        #    time.sleep(0.001)

        avgAx = int(avgAx / sampling_rate)
        avgAy = int(avgAy / sampling_rate)
        avgAz = int(avgAz / sampling_rate)
        avgGx = int(avgGx / sampling_rate)
        avgGy = int(avgGy / sampling_rate)
        avgGz = int(avgGz / sampling_rate)
        avgMx = int(avgMx / sampling_rate)
        avgMy = int(avgMy / sampling_rate)
        avgMz = int(avgMz / sampling_rate)

        dt = datetime.datetime.now(tz=datetime.timezone.utc)
        return {'date': dt.strftime('%Y/%m/%d'), 'time': (dt.strftime('%H:%M:%S.%f')[:-3]),
                'ax': avgAx, 'ay': avgAy, 'az': avgAz,
                'gx': avgGx, 'gy': avgGy, 'gz': avgGz,
                'mx': avgMx, 'my': avgMy, 'mz': avgMz}


    def write_to_csv(self, dict, fnames, file_name, write_header: bool = False):
        file = open(file_name, 'a+', newline='')
        with file:

            writer = csv.DictWriter(file, fieldnames=fnames)
            if write_header:
                writer.writeheader()
            for data_row in dict:
                writer.writerow(data_row)
        file.close()


    def run(self, run_for: int):

        t_end = time.time() + run_for
        fnames = ['date', 'time', 'ax', 'ay', 'az', 'gx', 'gy', 'gz', 'mx', 'my', 'mz']

        data = []

        while time.time() < t_end:
            data.append(self.get_imu_data())
            time.sleep(0.1)

        dt = datetime.datetime.now(tz=datetime.timezone.utc)
        date_string = dt.strftime('%Y-%m-%d-%H.%M-%S')
        filename = 'imu_log_{}.csv'.format(date_string)
        self.write_to_csv(data, fnames, self.file_path + filename, True)
        logging.info("Imu logging complete")
        return filename