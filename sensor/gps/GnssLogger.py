import logging

from ublox_gps import UbloxGps
import serial
import time
import datetime
import csv

# Can also use SPI here - import spidev
# I2C is not supported




class GnssLogger():


    def __init__(self, log_file_path: str):
        self.port = serial.Serial('/dev/ttyACM0', baudrate=38400, timeout=1)
        self.gps = UbloxGps(self.port)
        self.file_path = log_file_path

    def write_to_csv(self, dict, fnames, file_name, write_header: bool = False):
        file = open(file_name, 'a+', newline='')
        with file:
            writer = csv.DictWriter(file, fieldnames=fnames)
            if write_header:
                writer.writeheader()
            for data_row in dict:
                writer.writerow(data_row)
        file.close()


    def run(self, run_for: int):
        logging.info("bug me, run in gnss logger")

        t_end = time.time() + run_for
        fnames = ['date', 'time', 'longitude', 'latitude', 'heading']

        data = []
        try:
            while time.time() < t_end:
                try:
                    coords = self.gps.geo_coords()
                    dt = datetime.datetime.now(tz=datetime.timezone.utc)
                    dict = {'date': dt.strftime('%Y/%m/%d'), 'time': (dt.strftime('%H:%M:%S.%f')[:-3]),
                            'longitude': coords.lon, 'latitude': coords.lat, 'heading': coords.headMot}
                    data.append(dict)
                    time.sleep(0.1)
                except (ValueError, IOError) as err:
                        logging.error(err)
        finally:
            self.port.close()

        dt = datetime.datetime.now(tz=datetime.timezone.utc)
        date_string = dt.strftime('%Y-%m-%d-%H.%M-%S')
        filename = 'gnss_log_{}.csv'.format(date_string)
        self.write_to_csv(data, fnames, self.file_path + filename, True)
        logging.info("Gnss logging complete")
        return filename
