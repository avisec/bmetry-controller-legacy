#/bin/bash


#script to deploy the bmetry-controller to any host wanted.
#Deploys application
#installs dependencies
#starts application as systemD service


host=$1

root_dir="/home/pi/"
application_dir="$root_dir/bmetry-controller"


ssh pi@$host "mkdir $application_dir"

echo "Deploying application"
scp -r ./camera ./messaging ./sensor ./services ./app.py ./logger.py ./run.py ./requirements.txt pi@$host:$application_dir


#echo "Deploying config"
#scp ./application.properties pi@$host:$root_dir

#echo "Installing dependencies"
#ssh pi@$host "sudo apt update; sudo apt install -y python3 python3-pip"


#echo "Installing dependencies from requirements.txt"
#ssh pi@$host pip3 install -r $application_dir/requirements.txt

#echo "Installing systemD service"
#scp ./bmetry-controller.service pi@$host:$root_dir
#ssh pi@$host "sudo cp $root_dir/bmetry-controller.service /etc/systemd/system/; sudo systemctl daemon-reload; sudo systemctl enable bmetry-controller.service; sudo systemctl restart bmetry-controller.service; rm $root_dir/bmetry-controller.service"


