#/bin/bash


version="1.13"
package_name="bmetry-controller-$version-SNAPSHOT.zip"

zip -r $package_name ./camera ./messaging ./sensor ./services app.py logger.py requirements.txt run.py
