import json
import logging
import logger

from flask import Flask, request, jsonify

from camera.CaptureParameters import CaptureParameters
from services.CameraService import CameraService
from services.SmsService import SmsService
from services.ExifService import ExifService


app = Flask(__name__)

#app.logger.basicConfig(filename='controller.log', level=logging.DEBUG, format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')


camera_serice = CameraService()

@app.route('/')
def hello_world():
    logging.info("hello world!")
    return 'Hello World!'


@app.route('/sms', methods=['POST', 'GET'])
def forwarded_sms():
    if request.method == 'POST':
        text = request.form['message']
        sender = request.form['sender']
        logging.info("SMS command = {}. From {}".format(text, sender))
        if "bmetry" in text.lower():
            camera_serice.bmetry_sms(text, sender)
            return '', 200
        else:
            SmsService().send_sms(sender, "Unknown command: {}".format(text))
            return '', 410
    return '', 404


@app.route('/capture', methods=['POST'])
def capture():
        logging.info(request.data)
        json_data = json.loads(request.data)
        camera_params = CaptureParameters(**json_data['capture_parameters'])
        capture_mode = json_data['capture_mode']
        exif_service = ExifService(**json_data['exif_service'])
        logging.info("Capture params = {}".format(camera_params))
        logging.info("Capture mode = {}".format(capture_mode))
        try:
            camera_serice.exifService = exif_service
            response = camera_serice.capture_request(camera_params, capture_mode)
            return jsonify(response.__dict__), 200
        except:
            return '', 404

@app.route('/upload', methods=['GET'])
def upload_remaining():
        logging.info('Uploading images remaining on camera')
        capture_params = CaptureParameters(None, None, None, None, None, None, None, None, None).complete_with_defaults(
            None, None, None, None, None, None, None, None, None
        )
        try:
            camera_serice.capture_request(camera_params=capture_params, capture_mode="upload")
            return "Upload request started", 200
        except:
            return "Upload request failed"


"""
This is inteded for testing purposes
"""
@app.route('/collect-sensor-data', methods=['GET'])
def collect_sensor_data():
        logging.info('Collecting sensor data samples')
        try:
            camera_serice.collect_sensor_data(5)
            return "Sensor data collected and uploaded", 200
        except:
            return "Request for sensor data collection failed"








if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)

