from exif import Image
import time
import logging
from datetime import datetime


#Tag is valid for 2 hours to make sure the tag is added to the image even tough the connection breaks once.
class ExifService:

    dt_format = '%Y:%m:%d %H:%M:%S'

    def __init__(self, craneTag='', addedTime=1, expirySeconds=60*5, exifTagKey='user_comment'):
        self.craneTag = craneTag
        self.addedTime = addedTime
        self.expirySeconds = expirySeconds
        self.exifTagKey = exifTagKey

    def addTag(self, tagValue: str):
        self.craneTag = tagValue
        self.addedTime = datetime.now().timestamp()
        logging.info("Configured new custom exif tag: {}".format(tagValue))


    def addTagToImageIfNotExpired(self, image: Image, key: str, value: str) -> Image:
        if image.has_exif:
            image_make_time = datetime.strptime(image.datetime, self.dt_format)
            if image_make_time.timestamp() < (self.addedTime + self.expirySeconds):
                logging.info("Adding exif key: {}, value: {}".format(key, value))
                image.set(key, value)
            else:
                logging.info("Not setting exif. Last tag expired")
                image.set(key, "")
        return image


    def addCraneTag(self, array: bytearray) -> bytearray:
        image = Image(bytes(array))
        return bytearray(self.addTagToImageIfNotExpired(image, self.exifTagKey, self.craneTag).get_file())
