import logging
import os
import socket
from multiprocessing.dummy import Pool

import boto3
from botocore.exceptions import ClientError
import sensor.gps.GnssLogger as GnssLogger
import sensor.imu.ImuLogger as ImuLogger

import configparser


class SensorLoggerService():


    def __init__(self, properties_file_path='./application.properties'):

        config_parser = configparser.RawConfigParser()
        config_parser.read(properties_file_path)
        self.aws_id=config_parser.get('aws','aws_access_key_id')
        self.aws_secret=config_parser.get('aws','aws_secret_access_key')
        self.pool = Pool(2)
        self.sensor_log_file_path = '/tmp/'
        self.gnss_logger = GnssLogger.GnssLogger(self.sensor_log_file_path)
        self.imu_logger = ImuLogger.ImuLogger(self.sensor_log_file_path)



    bucket_name = 'bmetry-sensor-data'





    def logger_callback(self, context):
        logging.info("Starting {} upload".format(context))
        hostname = socket.gethostname()
        full_path = self.sensor_log_file_path + context
        if self.upload_file(file_name=full_path,
                         object_name="{}/{}".format(hostname, context)):
            os.remove(full_path)


    def start_logging(self, run_for: int):
        logging.info("Starting sensor logging")

        self.pool.apply_async(self.gnss_logger.run,
                              args=[run_for], callback=self.logger_callback)
        self.pool.apply_async(self.imu_logger.run,
                              args=[run_for], callback=self.logger_callback)


    def upload_file(self, file_name, object_name,):
        """Upload a file to an S3 bucket

        :param file_name: File to upload
        :param bucket: Bucket to upload to
        :param object_name: S3 object name. If not specified then file_name is used
        :return: True if file was uploaded, else False
        """

        # Upload the file
        s3_client = boto3.client('s3',
                                 aws_access_key_id=self.aws_id,
                                 aws_secret_access_key=self.aws_secret,
                                 )
        try:
            response = s3_client.upload_file(file_name, self.bucket_name, object_name)
        except ClientError as e:
            print(e)
            return False
        logging.info("Upload of {} complete".format(object_name))
        return True



