import configparser
from pathlib import Path
import logging

import logger

import requests



class SmsService():

    sms_service_url : str
    username :str
    password :str
    supervisors: list

    def __init__(self):
        sms_config = configparser.RawConfigParser()
        sms_config.read('./application.properties')
        self.sms_service_url = sms_config.get('bmetry','sms_endpoint')
        self.username = sms_config.get('bmetry','sms_username')
        self.password = sms_config.get('bmetry','sms_password')
        self.supervisors = sms_config.get('bmetry','sms_supervisor_list').split(',')

    def send_sms(self, receiver_number, message):
        parameters = {
            "username": self.username,
            "password": self.password,
            "number": self.standardize_number(receiver_number),
            "text": message
        }
        logging.info("Sending SMS to {} with message text {}".format(receiver_number, message))
        response = requests.get(self.sms_service_url, params=parameters)

    def send_sms_to_list(self, receiver_number_list: list, message):
        for receiver in receiver_number_list:
            self.send_sms(receiver, message)

    def send_sms_with_supervisor(self, receiver_number, message):
        receivers = [receiver_number]
        if self.supervisors[0] != '':
            for supervisor in self.supervisors:
                receivers.append(supervisor)
        self.send_sms_to_list(receivers, message)


    def standardize_number(self, number):
        if number[0] == '+':
            return '00' + number[1::]
        elif number[0] == '0' and number[1] != '0':
            return '0041' + number[1::]
        else: return number
