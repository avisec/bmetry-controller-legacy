import configparser
import json
import socket
import logging

import requests
from requests import Response

from messaging.CameraOperationDto import CameraOperationDto
from camera.CaptureParameters import CaptureParameters
from camera.controller import Controller
from messaging.ResponseMessage import ResponseMessage
from services import SensorLoggerService
from services.SmsService import SmsService
import re
from multiprocessing.dummy import Pool
from services.ExifService import ExifService
import os


class CameraService:

    properties_file = os.path.abspath("./application.properties")

    def __init__(self):
        self.sms_service = SmsService()
        controller_config = configparser.RawConfigParser()
        controller_config.read(self.properties_file)
        self.default_capture_duration=controller_config.get('bmetry','default_capture_duration')
        self.isMaster = controller_config.get('bmetry','is_master')
        self.list_of_slaves = controller_config.get('bmetry', 'list_of_slaves').split(',')
        self.response_list = []
        self.pool = Pool(10)
        self.futures = []
        self.sensor_logger_enabled = controller_config.get('bmetry', 'sensor_logger')
        if self.sensor_logger_enabled == 'enabled':
            self.sensor_logger = SensorLoggerService.SensorLoggerService(self.properties_file)
        self.exifService = ExifService()


    def bmetry_sms(self, text, sms_recipient):
        lower_text = text.lower()
        if "quickstart" in lower_text and "bmetry" in lower_text: self.quick_sms_capture(sms_recipient, text)
        elif "start" in lower_text and "bmetry" in lower_text: self.quick_sms_capture(sms_recipient, text)
        elif lower_text == "bmetry help" or lower_text == "bmetry h": self.send_help_text_sms(text, sms_recipient)
        else: self.parametrized_sms_capture(text, sms_recipient)


    def send_help_text_sms(self, text, sms_recipient):
        help_text = "Bmetry SMS control:\nQUICKSTART: 'bmetry quickstart': Capture a sequence of {} " \
                    "seconds.\n\nPARAMETRIZED EXAMPLE:\n'bmetry c=sequence shutterspeed=1/2000 aperture=4.5 iso=100 " \
                    "focus=no_focus'\n\nSINGLE IMAGE: 'bmetry c=single'".format(self.default_capture_duration)
        self.sms_service.send_sms(sms_recipient, help_text)


    def parametrized_sms_capture(self, text, sms_recipient):
        if "bmetry" in text.lower():
            capture_mode = self.get_argument_value(text.lower(), "capture_mode", "c")

            camera_params = self.get_capture_params_from_text(text.lower())

            exif_tag = self.get_argument_value(text, "tag", "tag")
            if exif_tag is not None:
                self.exifService.addTag(exif_tag)
            self.master_capture_request(camera_params, capture_mode, sms_recipient)
        else:
            SmsService().send_sms_with_supervisor(sms_recipient, "Unknown command: {}".format(text))
            raise Exception("Unknown command: {}".format(text))


    def master_capture_request(self, camera_params, capture_mode, sms_recipient):
            if capture_mode == None:
                self.sms_service.send_sms_with_supervisor(sms_recipient, "Unknown capture mode {}".format(capture_mode))
                raise Exception("Unknown capture mode {}".format(capture_mode))
            else:
                controller = Controller()
                try:
                    # send request to slaves
                    if self.isMaster:
                            self.forward_capture_request(capture_mode, camera_params, self.exifService)
                            logging.info("Capturing in mode: {}".format(capture_mode))
                            logging.info("Capturing with: {}".format(camera_params.__dict__))
                            camera = controller.prepare_capture(camera_params)
                            self.pool.apply_async(self.sms_service.send_sms_with_supervisor, args=[sms_recipient, "capture started"])
                            if capture_mode == 'sequence':
                                self.collect_sensor_data(camera_params.capture_duration)
                            nbr_of_files = controller.camera_capture(capture_mode, camera_params, camera, self.exifService)
                            # Add number of images
                            logging.info("capture finished successfully with {} images".format(nbr_of_files))
                            logging.info("Getting response from slaves")
                            self.response_list.append(ResponseMessage(socket.gethostname(), 'master', nbr_of_files, 'Capturing successfull'))
                            for r in self.response_list:
                                self.send_sms_report(sms_recipient, r)
                    else:
                        logging.info("Device is not set as Master")
                except Exception as e:
                    logging.info("capture failed: {}".format(e.with_traceback()))
                    self.sms_service.send_sms_with_supervisor(sms_recipient, "capture failed")
                finally:
                    self.response_list.clear()





    def capture_request(self, camera_params, capture_mode):
        if capture_mode == None:
            raise Exception("Unknown capture mode {}".format(capture_mode))
        else:
            controller = Controller()
            try:
                logging.info("Capturing in mode: {}".format(capture_mode))
                logging.info("Capturing with: {}".format(camera_params.__dict__))
                camera = controller.prepare_capture(camera_params)
                if capture_mode == 'sequence':
                    self.collect_sensor_data(camera_params.capture_duration)
                nbr_of_files = controller.camera_capture(capture_mode, camera_params, camera, self.exifService)
                # Add number of images
                logging.info("capture finished successfully with {} images".format(nbr_of_files))
                return ResponseMessage(socket.gethostname(), 'slave', nbr_of_files, "Capturing successfull")
            except Exception as e:
                logging.info("capture failed")
                return ResponseMessage(socket.gethostname(), 'slave', 0, "Capturing failed")



    def get_capture_params_from_text(self, text):
        focus_mode = self.get_argument_value(text, "focus_mode", "focus-mode")
        focus_area = self.get_argument_value(text, "focus_area", "focus-area")
        image_format = self.get_argument_value(text, "image_format", "imageformat")
        capture_duration = self.get_argument_value(text, "capture_duration", "duration")
        image_count = self.get_argument_value(text, "image_count", "image-count")
        break_time = self.get_argument_value(text, "break_time", "break-time")
        shutterspeed = self.get_argument_value(text, "shutterspeed", "shutterspeed")
        aperture = self.get_argument_value(text, "aperture", "aperture")
        iso = self.get_argument_value(text, "iso", "iso")

        return CaptureParameters(None, None, None, None, None, None, None, None, None).complete_with_defaults(
            focus_mode,
            focus_area,
            image_format,
            capture_duration,
            image_count,
            break_time,
            shutterspeed,
            aperture,
            iso,
        )

    def get_argument_value(self, text, key, key_alias):
        key_alias += '='
        try:
            sub_text = text.split(key)[1]
        except IndexError:
            try:
                sub_text = text.split(key_alias)[1]
            except IndexError:
                return None
        while ((sub_text[0] == '=') or (sub_text[0] == ' ')):
            sub_text = sub_text[1::]
        return re.split('\s|\t|\n|;', sub_text)[0]


    def quick_sms_capture(self, sms_recipient, text: str):
        split_text = text.split()
        if split_text[-1].lower() != ("quickstart" or "start"):
            self.exifService.addTag(split_text[-1])
        #camera prep
        camera_params = CaptureParameters(None, None, None, None, None, None, None, None, None).set_defaults()
        capture_mode = 'sequence'
        camera_params.capture_duration = int(self.default_capture_duration)
        #set area focus if definied in application.properties
        camera_params.focus_mode = 'area'
        self.master_capture_request(camera_params, capture_mode, sms_recipient)


    def forward_capture_request(self, capture_mode, camera_params, exifData: ExifService):
        dto = CameraOperationDto(capture_mode, camera_params, exifData)
        json_data = json.dumps(dto.__dict__)
        for slave in self.list_of_slaves:
            self.futures.append(
                self.pool.apply_async(requests.post, args=['http://{}/capture'.format(slave), json_data, ''],
                                      callback=self.on_success, error_callback=self.on_error))


    def send_sms_report(self, receiver, r_m: ResponseMessage):
        text = "Bmetry Capture report: \n camera: {}, number of images: {}".format(
            r_m.host, r_m.number_of_images
        )
        self.sms_service.send_sms_with_supervisor(receiver, text)

    def on_success(self, r: Response):
        if r.status_code == 200:
            logging.info(f'Post succeed: {r.json()}')
        else:
            logging.info(f'Post failed: {r}')
        self.response_list.append(ResponseMessage(**r.json()))

    def on_error(self, ex: Exception):
        logging.info(f'Post requests failed: {ex}')


    def collect_sensor_data(self, run_for:int):
        if self.sensor_logger_enabled == 'enabled':
            logging.info("Starting sensor logging")
            self.pool.apply_async(self.sensor_logger.start_logging, args=[run_for])
        else:
            logging.warn("Sensor logging disabled")