from camera.CaptureParameters import CaptureParameters
from services import ExifService

class CameraOperationDto:


    capture_mode: str
    capture_parameters: dict
    exif_service: dict


    def __init__(self, capture_mode:str, capture_parameters:CaptureParameters, exifService:ExifService):
        self.capture_mode = capture_mode
        self.capture_parameters = capture_parameters.__dict__
        self.exif_service = exifService.__dict__