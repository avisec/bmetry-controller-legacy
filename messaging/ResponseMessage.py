

class ResponseMessage:


    host: str
    device_function: str
    number_of_images: int
    message: str


    def __init__(self, host, device_function, number_of_images, message):
        self.host = host
        self.device_function = device_function
        self.number_of_images = number_of_images
        self.message = message